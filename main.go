package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/i-m-afk/chirpy/internal/database"
	"github.com/joho/godotenv"
)

type apiconfig struct {
	fileServerHits int
}

// request body structs
type params struct {
	Body string `json:"body"`
}
type user struct {
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`
	Expires  int    `json:"expires_in_seconds"`
}
type event struct {
	Event string `json:"event"`
	Data  struct {
		UserID int `json:"user_id"`
	} `json:"data"`
}

type refreshResp struct {
	Token string `json:"token"`
}

const errorMsg = "Something went wrong"

func (cfg *apiconfig) middlewareMetricInc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cfg.fileServerHits++
		next.ServeHTTP(w, r)
	})
}

func (cfg *apiconfig) numberOfRequest(w http.ResponseWriter) {
	hits := cfg.fileServerHits
	output := fmt.Sprintf("Hits: %d", hits)
	w.Write([]byte(output))
}

func (cfg *apiconfig) resetRequest(w http.ResponseWriter, req *http.Request) {
	cfg.fileServerHits = 0
	hits := cfg.fileServerHits
	output := fmt.Sprintf("Hits: %d", hits)
	w.Write([]byte(output))
}

func (cfg *apiconfig) htmlContent() []byte {
	return ([]byte(fmt.Sprintf("<html><body><h1>Welcome, Chirpy Admin</h1><p>Chirpy has been visited %d times!</p></body></html>", cfg.fileServerHits)))
}

func main() {
	godotenv.Load()
	jwtSecret := os.Getenv("JWT_SECRET")
	apiKey := os.Getenv("API_KEY")
	databaseName := flag.String("db", "./database.json", "database file path")
	debug := flag.Bool("debug", false, "Enable debug mode")
	flag.Parse()
	if *debug {
		deleteDatabase(*databaseName)
	}
	fmt.Println("server is starting... localhost:8080")
	r := chi.NewRouter()
	cfg := apiconfig{}
	db, err := database.NewDB(*databaseName)
	if err != nil {
		log.Fatal("database couldn't be created! ", err)
	}

	// middleware
	r.Use(middlewareLogger)
	r.Use(middlewareCors)
	r.Use(cfg.middlewareMetricInc)

	router := chi.NewRouter()
	adminRouter := chi.NewRouter()
	polkaRouter := chi.NewRouter()

	r.Handle("/app", http.StripPrefix("/app", http.FileServer(http.Dir("."))))
	r.Handle("/app/*", http.StripPrefix("/app/", http.FileServer(http.Dir("."))))
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: r,
	}

	router.Get("/healthz", func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text-plain; charset=utf-8")
		w.Write([]byte("OK"))
	})

	router.HandleFunc("/reset", cfg.resetRequest)
	adminRouter.Get("/metrics", func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Write(cfg.htmlContent())
	})

	r.Mount("/api", router)
	r.Mount("/admin", adminRouter)
	router.Mount("/polka", polkaRouter)

	router.Post("/chirps", func(w http.ResponseWriter, req *http.Request) {
		token := validToken(req)
		if token == "" {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		id, err := database.GetId(token, jwtSecret)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		decoder := json.NewDecoder(req.Body)
		para := params{}
		err = decoder.Decode(&para)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, errorMsg)
			return
		}
		if len(para.Body) <= 140 {
			respondWithJSON(w, http.StatusCreated, id, para, db)
		} else {
			respondWithError(w, http.StatusBadRequest, "Chirp is too long")
			return
		}
	})
	router.Get("/chirps", func(w http.ResponseWriter, r *http.Request) {
		sort := r.URL.Query().Get("sort")
		aid := r.URL.Query().Get("author_id")
		c, err := db.GetChirps(aid, sort)
		if err != nil {
			fmt.Println(err)
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(c)
	})
	router.Get("/chirps/{id}", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")
		integerId, err := strconv.Atoi(id)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		c, err := db.GetChirp(integerId)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		w.WriteHeader(http.StatusOK)
		if err = json.NewEncoder(w).Encode(c); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode("Something went wrong")
			return
		}
	})
	router.Post("/users", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		u := user{}
		if err := decoder.Decode(&u); err != nil {
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		user, err := db.CreateUser(u.Email, u.Password)
		var errSmallPass *database.ErrorPassTooSmall
		var errEmailExist *database.ErrorEmailExists
		switch {
		case err == nil:
			break
		case errors.As(err, &errSmallPass):
			respondWithError(w, http.StatusBadRequest, "Password is too short")
			return
		case errors.As(err, &errEmailExist):
			respondWithError(w, http.StatusBadRequest, "Email already exists")
			return
		default:
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(user)
	})
	router.Post("/login", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		u := user{}
		if err := decoder.Decode(&u); err != nil {
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		res, err := db.AuthenticateUser(u.Email, u.Password, jwtSecret, u.Expires)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, "Something went wrong")
			return
		}
		if len(res.Email) == 0 {
			respondWithError(w, http.StatusUnauthorized, "Password or username not correct")
			return
		}
		json.NewEncoder(w).Encode(res)
	})
	router.Put("/users", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		u := user{}
		if err := decoder.Decode(&u); err != nil {
			respondWithError(w, http.StatusInternalServerError, errorMsg)
			return
		}
		token := validToken(r)
		if token == "" {
			respondWithError(w, http.StatusUnauthorized, "Missing authorization header")
			return
		}
		res, err := db.UpdateCred(u.Email, u.Password, token, jwtSecret)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, "Something went wrong")
			return
		}
		if len(res.Email) == 0 {
			respondWithError(w, http.StatusUnauthorized, "Password or username not correct")
			return
		}
		json.NewEncoder(w).Encode(res)
	})
	router.Post("/refresh", func(w http.ResponseWriter, r *http.Request) {
		token := validToken(r)
		refreshToken, err := db.RefreshToken(token, jwtSecret)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(refreshToken)
	})
	router.Post("/revoke", func(w http.ResponseWriter, r *http.Request) {
		token := validToken(r)
		_, err := database.VerifyToken(token, jwtSecret)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		err = db.RevokeToken(token)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		w.WriteHeader(http.StatusOK)
	})
	router.Delete("/chirps/{chirpID}", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "chirpID")
		token := validToken(r)
		err := db.DeleteChirp(id, token, jwtSecret)
		if err != nil {
			respondWithError(w, http.StatusUnauthorized, "Invalid token")
			return
		}
		w.WriteHeader(http.StatusOK)
	})
	polkaRouter.Post("/webhooks", func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		key := getApiFromHeader(r)
		p := event{}
		if err := decoder.Decode(&p); err != nil {
			respondWithError(w, http.StatusNotFound, errorMsg)
			return
		}

		if p.Event != "user.upgraded" {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		fmt.Println("key: ", key)
		if err := db.AddSubscription(p.Data.UserID, key, apiKey); err != nil {
			if err.Error() == "invalid api key" {
				respondWithError(w, http.StatusUnauthorized, err.Error())
				return
			}
			respondWithError(w, http.StatusNotFound, "not found")
			return
		}
		w.WriteHeader(http.StatusOK)
	})
	ok := server.ListenAndServe()
	if ok != nil {
		fmt.Println(ok)
	}
}

func getApiFromHeader(r *http.Request) string {
	token := r.Header.Get("Authorization")
	if token == "" {
		return ""
	}
	if !strings.Contains(token, "ApiKey ") {
		return ""
	}
	token = token[len("ApiKey "):]
	return token
}

func validToken(r *http.Request) string {
	token := r.Header.Get("Authorization")
	if token == "" {
		return ""
	}
	if !strings.Contains(token, "Bearer ") {
		return ""
	}
	token = token[len("Bearer "):]
	return token
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]string{"error": msg})
}

func respondWithJSON(w http.ResponseWriter, code, id int, payload interface{}, db *database.DB) {
	badwords := map[string]bool{"kerfuffle": true, "sharbert": true, "fornax": true}

	payloadString := fmt.Sprintf("%v", payload.(params).Body)
	words := strings.Split(payloadString, " ")
	for i, word := range words {
		if badwords[word] {
			words[i] = "****"
		}
	}
	w.WriteHeader(code)
	c, err := db.CreateChirp(strings.Join(words, " "), id)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, errorMsg)
		return
	}
	json.NewEncoder(w).Encode(c)
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func middlewareLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v %v", r.Method, r.URL)
		next.ServeHTTP(w, r)
	})
}

func deleteDatabase(dbname string) {
	if err := os.Remove(dbname); err != nil {
		log.Println(err)
		return
	}
	log.Printf("Database file %s is deleted", dbname)
}
