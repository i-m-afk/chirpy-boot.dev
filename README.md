# Chirpy - A simple social networking server

My complete implementation of chirpy, using assignments from [Learn Web Servers](https://www.boot.dev/assignments/50f37da8-72c0-4860-a7d1-17e4bda5c243) of boot.dev course.
This is a server for a social networking site called Chirpy. It serves as a simple REST API.
## Getting started 🚀

### Environment Configuration
Setup a `.env` file with the following values:
```
"JWT_SECRET": "your-secret"
"API_KEY": "your-api-key"
```
### Running the application
1. Clone the repository and navigate to the project directory.
2. Setup the environment variables.
3. Run the server using `go build && ./chirpy` server will run on `localhost:8080`
3.1. You can also use `--debug` flag to run in debug mode. eg:  `go build && ./chirpy --debug true`, 
this will delete your test database to enable fresh restart.
3.2. You can specify the database name using `--db` flag. eg: `go build && ./chirpy --db chirpy.json`

## API Endpoints
    
    GET /app: Serve a welcome html page.
    GET /admin/metrics: Shows the hit counts of the server.
    GET /reset: Reset the hit counts of the server.
    GET /api/chirps: Retrieve all chirps.
    - URL QUERY, GET /api/chirps?sort="desc" or GET /api/chirps?sort="asc"
    - URL QUERY, GET /api/chirps?author_id=1
    POST /api/chirps: Create a new chirp for logged in user.
    GET /api/chirps/{id}: Retrieve a chirp by ID.
    DELETE /api/chirps/{chirpID}: Delete a chirp by ID.
    POST /login: Log in with email and password, Get JWT token and refresh token.
    POST /users: Register a new user.
    PUT /api/users/
    POST /refresh: Refresh an expired JWT token.
    POST /revoke: Revoke an active JWT token (blacklist a jwt).
    GET /healthz: Health check endpoint.
    POST /polka/webhooks: Handle webhook events.
And more,  I don't want to document xD explore the assignments

## License
MIT
