package database

import (
	"fmt"
)

type ErrorPassTooSmall struct {
	Length int
}

func (e ErrorPassTooSmall) Error() string {
	return fmt.Sprintf("password must be at least %d characters long", e.Length)
}

func NewErrorPassTooSmall(length int) *ErrorPassTooSmall {
	return &ErrorPassTooSmall{Length: length}
}

type ErrorEmailExists struct {
	email string
}

func (e ErrorEmailExists) Error() string {
	return fmt.Sprintf("email already in use %s", e.email)
}

func NewErrorEmailExists(email string) *ErrorEmailExists {
	return &ErrorEmailExists{email: email}
}

type ErrorInvalidToken struct {
	token string
}

func (e ErrorInvalidToken) Error() string {
	return "token is not valid"
}

func NewErrorInvalidToken(token string) *ErrorInvalidToken {
	return &ErrorInvalidToken{token: token}
}

type ErrorParseClaim struct {
	token string
}

func (e ErrorParseClaim) Error() string {
	return "couldn't parse claims"
}

func NewErrorParseClaims(token string) *ErrorParseClaim {
	return &ErrorParseClaim{token: token}
}
