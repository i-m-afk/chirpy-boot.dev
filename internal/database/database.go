package database

import (
	"cmp"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

type DB struct {
	mux  *sync.RWMutex
	path string
}

type DBStruct struct {
	Chirps        map[int]Chirp   `json:"chirps"`
	Users         map[int]User    `json:"users"`
	RevokedTokens map[string]bool `json:"revoked_tokens"`
}

type Chirp struct {
	Body     string `json:"body"`
	Id       int    `json:"id"`
	AuthorId int    `json:"author_id"`
}

type User struct {
	Email        string `json:"email"`
	Password     string `json:"password,omitempty"` // encrypted password
	Token        string `json:"token,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
	Id           int    `json:"id"`
	Sub          bool   `json:"is_chirpy_red"`
}

type MyCustomClaims struct {
	Type string `json:"type"`
	jwt.RegisteredClaims
}

func NewDB(path string) (*DB, error) {
	db := &DB{path: path, mux: &sync.RWMutex{}}
	err := db.ensureDB()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func (db *DB) ensureDB() error {
	_, err := os.OpenFile(db.path, os.O_RDWR|os.O_CREATE, 0777)
	return err
}

// creates a new chirp and save it to disk
func (db *DB) CreateChirp(body string, id int) (Chirp, error) {
	db.mux.Lock()
	defer db.mux.Unlock()

	jsonDb, err := db.loadDB()
	if err != nil {
		return Chirp{}, err
	}
	l := len(jsonDb.Chirps)
	chirp := Chirp{Body: body, Id: l + 1, AuthorId: id}

	jsonDb.Chirps[l+1] = chirp

	if err = db.writeDB(jsonDb); err != nil {
		return Chirp{}, err
	}
	return chirp, nil
}

func (db *DB) GetChirps(authorID, sort string) ([]Chirp, error) {
	if authorID != "" {
		return db.getChirpsByAuthorID(authorID, sort)
	}
	dbstr, err := db.loadDB()
	if err != nil {
		return nil, err
	}

	var chirps []Chirp
	for _, v := range dbstr.Chirps {
		chirps = append(chirps, v)
	}
	return sortChirp(chirps, sort), nil
}

func (db *DB) getChirpsByAuthorID(authorID, sort string) ([]Chirp, error) {
	aid, err := strconv.Atoi(authorID)
	if err != nil {
		return nil, err
	}
	dbstr, err := db.loadDB()
	if err != nil {
		return nil, err
	}
	chirps := dbstr.Chirps
	buffer := make([]Chirp, 0, 1024)
	for _, c := range chirps {
		if c.AuthorId == aid {
			buffer = append(buffer, c)
		}
	}
	return sortChirp(buffer, sort), nil
}

func sortChirp(chirps []Chirp, sort string) []Chirp {
	if sort == "asc" {
		return sortAscending(chirps)
	}
	if sort == "desc" {
		return sortDescending(chirps)
	}
	return chirps
}

func sortAscending(chirps []Chirp) []Chirp {
	slices.SortFunc(chirps, func(a, b Chirp) int {
		return cmp.Compare(a.Id, b.Id)
	})
	return chirps
}

func sortDescending(chirps []Chirp) []Chirp {
	slices.Reverse(sortAscending(chirps))
	return chirps
}

func (db *DB) GetChirp(id int) (Chirp, error) {
	chirps, err := db.GetChirps("", "")
	if err != nil {
		return Chirp{}, err
	}

	for _, v := range chirps {
		if v.Id == id {
			return v, nil
		}
	}
	return Chirp{}, errors.New("requested chirp does not exist")
}

// reads database file from disk
func (db *DB) loadDB() (DBStruct, error) {
	file, err := os.ReadFile(db.path)
	// if json file is empty, return empty db
	if len(file) == 0 {
		return DBStruct{Chirps: make(map[int]Chirp), Users: make(map[int]User), RevokedTokens: make(map[string]bool)}, nil
	}
	data := DBStruct{}
	if err != nil {
		return data, err
	}
	err = json.Unmarshal(file, &data)
	if err != nil {
		return data, err
	}

	// check if chirps and users are empty
	if len(data.Chirps) == 0 {
		data.Chirps = make(map[int]Chirp)
	}
	if len(data.Users) == 0 {
		data.Users = make(map[int]User)
	}
	if len(data.RevokedTokens) == 0 {
		data.RevokedTokens = make(map[string]bool)
	}
	return data, nil
}

// make file empty
func emptyFile(filename string) error {
	// truncate file
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	return nil
}

// writes database file to disk
func (db *DB) writeDB(dbStructure DBStruct) error {
	file, err := os.OpenFile(db.path, os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	// delete all the content from the file to rewrite it (HACK)
	if err := emptyFile(db.path); err != nil {
		return err
	}
	defer file.Close()
	jsonDb, err := json.Marshal(dbStructure)
	if err != nil {
		return err
	}
	_, err = file.WriteString(string(jsonDb))
	if err != nil {
		return err
	}
	// Flush data to disk
	if err := file.Sync(); err != nil {
		return err
	}
	return nil
}

// creates a new user and save it to disk
func (db *DB) CreateUser(email string, password string) (User, error) {
	db.mux.Lock()
	defer db.mux.Unlock()
	if !validatePassword(password) {
		return User{}, NewErrorPassTooSmall(len(password))
	}
	encrypt, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return User{}, nil
	}
	jsonDb, err := db.loadDB()
	if err != nil {
		return User{}, err
	}
	for _, v := range jsonDb.Users {
		if v.Email == email {
			return User{}, NewErrorEmailExists(email)
		}
	}

	l := len(jsonDb.Users)
	user := User{Id: l + 1, Email: email, Password: string(encrypt)}

	jsonDb.Users[l+1] = user

	if err = db.writeDB(jsonDb); err != nil {
		return User{}, err
	}
	return User{Id: user.Id, Email: user.Email, Sub: user.Sub}, nil
}

func validatePassword(password string) bool {
	return len(password) >= 6
}

func (db *DB) isValidUser(email, password string) (User, bool, error) {
	data, err := db.loadDB()
	if err != nil {
		return User{}, false, err
	}
	users := data.Users

	for _, user := range users {
		if user.Email == email {
			if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
				return User{}, false, err
			}
			return user, true, nil
		}
	}
	return User{}, false, nil
}

// access-token
const (
	accessIssuer       = "chirpy-acess"
	claimType          = "jwt-claim"
	accessTokenExpiry  = time.Hour
	refreshTokenExpiry = 60 * 24 * time.Hour
	refreshIssuer      = "chirpy-refresh"
)

// send token, refresh_token, id and email
func (db *DB) AuthenticateUser(email, password, secret string, expires_in_seconds int) (User, error) {
	user, ok, err := db.isValidUser(email, password)
	if err != nil {
		return User{}, err
	}
	if !ok {
		return User{}, nil
	}
	claims := generateClaims(user.Id, expires_in_seconds, email, accessIssuer)
	token, err := generateToken(claims, secret)
	if err != nil {
		return User{}, err
	}
	refreshClaims := generateClaims(user.Id, expires_in_seconds, email, refreshIssuer)
	refreshToken, err := generateToken(refreshClaims, secret)
	if err != nil {
		return User{}, err
	}
	return User{Email: user.Email, Id: user.Id, Token: token, RefreshToken: refreshToken, Sub: user.Sub}, nil
}

func generateClaims(userID, expires_in_seconds int, email, issuer string) jwt.Claims {
	now := time.Now()
	expiration := now.Add(time.Duration(expires_in_seconds) * time.Second)
	if expires_in_seconds == 0 && issuer == accessIssuer {
		expiration = now.Add(accessTokenExpiry)
	}
	if expires_in_seconds == 0 && issuer == refreshIssuer {
		expiration = now.Add(refreshTokenExpiry)
	}
	return MyCustomClaims{
		claimType,
		jwt.RegisteredClaims{
			Issuer:    issuer,
			IssuedAt:  &jwt.NumericDate{Time: now},
			ExpiresAt: &jwt.NumericDate{Time: expiration},
			Subject:   fmt.Sprint(userID, ",", email), // better way is to use uuid for ids
		},
	}
}

func generateToken(claims jwt.Claims, secret string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secret))
}

// get id from token string
func GetId(token, secret string) (int, error) {
	claims, err := VerifyToken(token, secret)
	if err != nil {
		return -1, err
	}
	subject := claims["sub"].(string) // Subject string `json:"sub,omitempty"`

	idx := strings.IndexByte(subject, ',')
	subjectId := subject[:idx]
	id, err := strconv.Atoi(subjectId)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// Update email and password
func (db *DB) UpdateCred(email, password, token, secret string) (User, error) {
	data, err := db.loadDB()
	if err != nil {
		return User{}, err
	}
	claims, err := VerifyToken(token, secret)
	if err != nil {
		return User{}, NewErrorInvalidToken(token)
	}
	subject := claims["sub"].(string) // Subject string `json:"sub,omitempty"`

	idx := strings.IndexByte(subject, ',')
	subjectId := subject[:idx]
	subjectEmail := subject[idx+1:]
	id, err := strconv.Atoi(subjectId)
	if err != nil {
		return User{}, err
	}
	if data.Users[id].Email != subjectEmail {
		log.Println("token email recieved  missmatched: ", subjectEmail)
		return User{}, NewErrorInvalidToken(token)
	}

	encrypt, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return User{}, err
	}

	delete(data.Users, id)

	data.Users[id] = User{Email: email, Password: string(encrypt), Id: id}
	if err := db.writeDB(data); err != nil {
		return User{}, err
	}
	return User{Email: email, Id: id, Sub: data.Users[id].Sub}, nil
}

func VerifyToken(tokenstr, secret string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenstr, func(t *jwt.Token) (interface{}, error) {
		if t.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, NewErrorInvalidToken(tokenstr)
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, NewErrorParseClaims(tokenstr)
	}
	issuer, err := claims.GetIssuer()
	if err != nil {
		return nil, err
	}
	if issuer != accessIssuer {
		return nil, NewErrorInvalidToken(tokenstr)
	}
	return claims, nil
}

func verifyRefreshToken(tokenstr, secret string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tokenstr, func(t *jwt.Token) (interface{}, error) {
		if t.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, NewErrorInvalidToken(tokenstr)
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, NewErrorParseClaims(tokenstr)
	}
	issuer, err := claims.GetIssuer()
	if err != nil {
		return nil, err
	}
	if issuer != refreshIssuer {
		return nil, NewErrorInvalidToken(tokenstr)
	}
	return claims, nil
}

func (db *DB) RefreshToken(refreshToken, secret string) (string, error) {
	// verify refresh token
	claims, err := verifyRefreshToken(refreshToken, secret)
	if err != nil {
		return "", err
	}
	ok, err := db.checkExclusionRefreshToken(refreshToken)
	if !ok {
		return "", NewErrorInvalidToken(refreshToken)
	}
	if err != nil {
		return "", err
	}
	// passed, create new jwt

	// get id
	subject := claims["sub"].(string) // Subject string `json:"sub,omitempty"`

	idx := strings.IndexByte(subject, ',')
	subjectId := subject[:idx]
	id, err := strconv.Atoi(subjectId)
	if err != nil {
		return "", err
	}
	// HACKS BAD PRACTICE
	jsonDB, err := db.loadDB()
	if err != nil {
		return "", err
	}
	newClaim := generateClaims(id, 0, jsonDB.Users[id].Email, accessIssuer)
	token, err := generateToken(newClaim, secret)
	if err != nil {
		return "", err
	}
	return token, nil
}

func (db *DB) checkExclusionRefreshToken(token string) (bool, error) {
	jsonDb, err := db.loadDB()
	if err != nil {
		return false, err
	}
	tokens := jsonDb.RevokedTokens
	ok := tokens[token]
	if !ok {
		return true, nil
	}
	return false, nil
}

func (db *DB) RevokeToken(token string) error {
	jsonDb, err := db.loadDB()
	if err != nil {
		return err
	}
	jsonDb.RevokedTokens[token] = true
	if err = db.writeDB(jsonDb); err != nil {
		return err
	}
	return nil
}

func (db *DB) DeleteChirp(i, token, secret string) error {
	id, err := strconv.Atoi(i)
	if err != nil {
		return err
	}
	jsonDb, err := db.loadDB()
	if err != nil {
		return err
	}
	userId, err := GetId(token, secret)
	if err != nil {
		return err
	}
	if userId != jsonDb.Chirps[id].AuthorId {
		return errors.New("unauthorized user")
	}
	fmt.Println(jsonDb)
	delete(jsonDb.Chirps, id)
	fmt.Println(jsonDb)
	if err := db.writeDB(jsonDb); err != nil {
		return err
	}
	return nil
}

func (db *DB) AddSubscription(id int, key, apiKey string) error {
	if key != apiKey {
		return errors.New("invalid api key")
	}
	db.mux.Lock()
	defer db.mux.Unlock()
	jsonDb, err := db.loadDB()
	if err != nil {
		return err
	}
	user := jsonDb.Users[id]
	user.Sub = true

	delete(jsonDb.Users, id)
	jsonDb.Users[id] = user
	if err = db.writeDB(jsonDb); err != nil {
		return err
	}
	return nil
}
